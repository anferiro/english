# Hermes System

### A Transportation Tracking System

---
## Problem
Colombia has two ways to transport people and goods, one is the public transportation who mainly transports people and it is widely cover and control by the government, and the other one is the special transportation who not only transports people but goods as well. we are going to focus on the special transportation who covers. 


---
- School transportation
- Employees transportation
- Truism transportation
- Health transportation
---

## What are the problems that people in Colombia has today in this regards? 

---
 
Imagine wait for hours for your kids without having a clue where they are and how

---

Imagine that the school bus arrived and your kid is not there

---

Imagine that you send an important package to your client but actually you can not located the package and your client is angry and expecting for that package 

---

## Value Proposition

---

Hermes Track is a transportation tracking system that allows to know in real time how, when, who, and where are the goods and/or people who are being transported from one place to another. This system builds high confidence in your clients making transparent every single event during the transportation and offers the opportunity to follow the cargo using real time video, also it saves money and effort managing automobiles inventory, integrating with governance systems.

---
## Benefits

---
Making easy to monitor and track shipments in real time
- Video Streaming
- Bidirectional communication system

---
Saving time facilitating the Route planing

---
Building confidence for the final customer

---
Saving cost and effort managing automobiles inventory

---
Saving cost and effort integrating with governance platforms 

---
Saving cost maintaining a technology infrastructure
